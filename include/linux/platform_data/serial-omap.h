/*
 * Driver for OMAP-UART controller.
 * Based on drivers/serial/8250.c
 *
 * Copyright (C) 2010 Texas Instruments.
 *
 * Authors:
 *	Govindraj R	<govindraj.raja@ti.com>
 *	Thara Gopinath	<thara@ti.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

#ifndef __OMAP_SERIAL_H__
#define __OMAP_SERIAL_H__

#include <linux/serial_core.h>
#include <linux/device.h>
#include <linux/pm_qos.h>
//#include "mux.h"

#include <linux/workqueue.h>
#define DRIVER_NAME	"omap_uart"
#define OMAP_MODE13X_SPEED	230400

#define OMAP_UART_ISR2			0X1C
#define OMAP_UART_RXFIFO_EMPTY_STS	0X01
#define OMAP_UART_TXFIFO_EMPTY_STS	0X02

#define OMAP_UART_SCR_RXTRIGGRANU1 0x80
#define OMAP_UART_SCR_TXTRIGGRANU1 0x40
#define OMAP_UART_SCR_TX_EMPTY	   0x08
#define OMAP_UART_DMA_CH_FREE	-1

#define OMAP_MAX_HSUART_PORTS	6
#define OMAP_UART_WER_MOD_WKUP	0X7F
#define OMAP_UART_SW_CLR	0XF0
/* Enable XON/XOFF flow control on output */
#define OMAP_UART_SW_TX		0x04

/* Enable XON/XOFF flow control on input */
#define OMAP_UART_SW_RX		0x04
#define OMAP_UART_TCR_TRIG	0X0F
#define OMAP_UART_SW_CLR	0XF0
#define UART_ERRATA_i202_MDR1_ACCESS	BIT(0)
#define UART_ERRATA_i291_DMA_FORCEIDLE	BIT(1)
/*
 * Use tty device name as ttyO, [O -> OMAP]
 * in bootargs we specify as console=ttyO0 if uart1
 * is used as console uart.
 */
#define OMAP_SERIAL_NAME	"ttyO"

struct omap_uart_port_info {
	bool			dma_enabled;	/* To specify DMA Mode */
	unsigned int		uartclk;	/* UART clock rate */
	upf_t			flags;		/* UPF_* flags */
	unsigned int		dma_rx_buf_size;
	unsigned int		dma_rx_timeout;
	unsigned int		autosuspend_timeout;
	unsigned int		dma_rx_poll_rate;
	int			DTR_gpio;
	int			DTR_inverted;
	int			DTR_present;

	int (*get_context_loss_count)(struct device *);
	void (*set_forceidle)(struct device *);
	void (*set_noidle)(struct device *);
	void (*enable_wakeup)(struct device *, bool);
};

#endif /* __OMAP_SERIAL_H__ */
